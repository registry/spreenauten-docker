#!/usr/bin/env sh

REPO=registry.gitlab.com/registry/spreenauten-docker

set -e

. images

if [ ! -z "$1" ]; then IMAGES="$1"; fi

for IMAGE in $IMAGES; do
	docker push ${REPO}:$IMAGE
done

