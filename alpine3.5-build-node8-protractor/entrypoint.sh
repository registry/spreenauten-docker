#!/bin/sh

if [ -f "/protractor/package.json" ]; then
    npm install
fi

exec protractor $@
