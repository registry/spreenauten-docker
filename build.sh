#!/usr/bin/env sh

REPO=registry.gitlab.com/registry/spreenauten-docker
set -e

. images
if [ ! -z "$1" ]; then IMAGES="$1"; fi

ROOT_DIRECTORY=`pwd`
for IMAGE in $IMAGES; do
	echo -e "\e[36mBuilding image \e[1m$IMAGE\e[0m"
	cd $ROOT_DIRECTORY/$IMAGE
	docker pull ${REPO}:$IMAGE || docker build -t ${REPO}:$IMAGE .
done
